module WebfaceComponentHelper

  def webface(tag_name, component: nil, options: {}, attrs: nil)

    # DEPRECATED v0.1.8 backwards compatability support with a warning
    if attrs
      options = options.merge(attrs)
      Rails.logger.warn "  Webface.js DEPRECATION WARNING: use of `attrs` argument is deprecated, use `options` instead\n" +
                        "    (tag: #{tag_name}, component: #{component}, html: #{options.inspect})"
    end

    corresponding_component_html_attrs = {}

    # This is for cases when we don't want to call a block
    # on component* methods and instead want to just pass a string
    # that will be the contents of that html tag.
    text_content = options.delete(:text_content)

    component          = "#{component.to_s.camelize}Component" unless component.nil?
    options            = create_attr_map(options) if options[:mapped_attrs]
    options[:attr_map] = prepare_attr_map(options[:attr_map])

    read_attr_map(options[:attr_map]).each do |attr, html_attr|
      corresponding_component_html_attrs[html_attr] = options.delete(attr)
    end

    # convert short data attribute names to proper longer ones
    webface_data = {}
    webface_data[:component_class]    = component
    webface_data[:component_template] = webface_data.delete(:component_class) if options[:_template]

    # DEPRECATED v0.1.8
    webface_data[:component_attr]     = get_new_or_deprecated_attr_and_warn(:property, :attr, options, tag_name)

    webface_data[:component_part]     = options.delete(:part)
    webface_data[:component_roles]    = options.delete(:roles)
    webface_data[:component_attr_map] = get_new_or_deprecated_attr_and_warn(:property_attr, :attr_map, options, tag_name)
    webface_data[:component_display_value] = options.delete(:display)

    # DEPRECATED v0.1.8 option fix, to be removed along with other DEPRECATION warnings;
    #   applies to :property_attr & :property
    # delete those attrs above when assigning to webface_data.
    [:property_attr, :property, :attr, :attr_map, :model].each do |a|
      options.delete(a)
    end

    # Attributes that start with _ are probably important to some of the methods in this module,
    # but they're not important to html, so we strip them. One such example would be the _name
    # attribute. This attribute comminucates model field name to fetch model errors for this field,
    # but we don't need this to be an html attribute on the component itself, since actual html form
    # fields are normally wrapped in divs in webface_rails views.
    options.delete_if { |k,v| k.to_s.starts_with?("_") }

    tag_attrs = { data: webface_data.merge(options.delete(:data) || {})}.merge(corresponding_component_html_attrs).merge(options)
    content_tag(tag_name, tag_attrs) do
      block_given? ? yield : text_content
    end
  end

  def component(name, options={}, &block)
    render partial: "webface_components/#{name}", locals: { options: options, block: block }
  end

  def component_template(name, options={}, &block)
    component(name, options.merge({ _template: true }), &block)
  end

  def component_block(component, tag_name="div", component_options={}, html_options={}, &block)
    options           = component_options.merge(html_options)
    attr_map          = options.delete(attr_map)
    tag_name, options = derive_tag_and_css_class(tag_name, options)

    component = nil if options[:component_template]

    # If block represent a form field, let's see if there are ActiveRecord error messages
    # and mark the block with appropriate css class.
    options[:form_errors_class] ||= true
    if options.delete(:form_errors_class) && options[:model]
      options[:class] = [
        options[:class],
        (get_error_for_field_name(options).blank? ? nil : "errors")
      ].compact.join(" ")
    end

    webface(tag_name, component: component, options: options, &block)
  end
  alias :define_component :component_block

  def component_attr(attr_name, tag_name="span", component_options={}, html_options={}, &block)
    component_block(nil, tag_name, component_options.merge({ attr: attr_name }), html_options, &block)
  end
  def property(attr_name, tag_name="span", component_options={}, html_options={}, &block) # DEPRECATED v0.1.8
    Rails.logger.warn "  Webface.js DEPRECATION WARNING: use of `#property` helper is deprecated, use `#attr` instead\n    (tag: #{tag_name}, attr name: #{attr_name})"
    self.component_attr(attr_name, tag_name, component_options, html_options, &block)
  end

  def component_part(part_name, tag_name="div", component_options={}, html_options={}, &block)
    component_block(nil, tag_name, component_options.merge({ part: part_name }), html_options, &block)
  end

  def button_link(caption, path, options={}, verb="GET")
    if verb.upcase == "get"
      component_block (options[:confirmation] ? "confirmable_button" : "button"), "a", { class: "button", href: path, data: { prevent_native_click_event: "false", component_attr_map: "lockable,disabled,confirmation,prevent_native_click_event", component_attr: "caption" }}.merge(options) do
        caption
      end
    else
      render partial: "webface_components/post_button_link", locals: { caption: caption, path: path, verb: verb.upcase, options: options }
    end
  end

  def post_button_link(caption, path, options={}, verb)
    if options.kind_of?(String)
      # DEPRECATED v0.1.8, swap arguments order, verb goes last.
      _verb = options; options = verb; verb = _verb
      Rails.logger.warn "  Webface.js DEPRECATION WARNING: when calling WebfaceComponentHelper#post_button_link\n" +
                        "    verb argument should be the 4th, not the 3rd."
    end
    button_link(caption, path, options, verb)
  end

  def webface_form(model, action=nil, attrs={}, &block)

    if model.new_record?
      action = send("#{model.class.to_s.underscore.pluralize}_path") unless action
      method_field = ""
    else
      action = send("#{model.class.to_s.underscore}_path", model.to_param) unless action
      method_field = content_tag(:input, "", type: "hidden", name: "_method", value: "PATCH")
    end

    method_field = content_tag(:input, "", type: "hidden", name: "_method", value: attrs.delete(:method)) if attrs[:method]
    no_labels    = attrs.delete(:no_labels)

    auth_token_field = content_tag(:input, "", value: form_authenticity_token, type: "hidden", name: "authenticity_token")

    f = WebfaceForm.new(model, self, no_labels: no_labels)
    content = capture(f, &block)
    content_tag(:form, { action: action, method: 'POST', "accept-charset" => "UTF-8", "enctype" => "multipart/form-data" }.merge(attrs)) do
      concat content
      concat method_field
      concat auth_token_field
    end
  end
  alias :component_form :webface_form

  def prepare_select_collection(c, blank_option: false)

    if c
      collection = c.dup
    else
      return []
    end

    if collection.kind_of?(Array)
      if !collection[0].kind_of?(Array)
        collection.map! { |option| [option, option] }
      end
    elsif collection.kind_of?(Hash)
      collection = collection.to_a
    end

    if blank_option
      collection.insert(0,["null", t("views.webface.select.no_value")])
    end

    collection.map! { |option| [option[0].to_s, option[1].to_s] }
    collection

  end

  def select_component_get_selected_value(options, type=:input)

    input_value = if options[:selected]
      options[:selected]
    elsif options[:_name]
      get_value_for_field_name(options)
    end

    if type == :input
      input_value
    else
      if options[:collection].blank? && !get_value_for_field_name(options)
        return get_value_for_field_name(options, from_params: true)
      end
      prepare_select_collection(options[:collection]).to_h[input_value]
    end

  end

  def get_value_for_field_name(options, from_params: false)
    return options[:value] if options[:value]

    @model_array_fields_counter ||= {}

    if options[:_name] && options[:_name].include?("[")
      names       = get_field_names_chain_from_nested_field_param_and_model(options)
      model_name  = names.shift

      if options[:model] && !from_params
        field_name = options[:_model_field_name]
        if field_name.blank?
          return nil
        else
          if options[:_nested_field]
            model = get_target_model(names, options)
            model.send(field_name) if model
          else
            field = options[:model].send(names.shift)
            names.each do |n|
              return nil if n.nil? || field.nil?
              # This deals with fields that are arrays, for example quiz_question[answers][en][]
              # In case field name ends with [] (empty string in `n`), we start counting how many fields
              # with such a name exist and pull values from the model's array stored in that field.
              if n.to_s == ""
                @model_array_fields_counter[options[:_name]] ||= 0
                field = field[@model_array_fields_counter[options[:_name]]]
                @model_array_fields_counter[options[:_name]] = @model_array_fields_counter[options[:_name]] + 1
              else
                field = field[n]
              end
            end
            return field
          end
        end
      else
        value = params[options[:_model_name]]
        names.each do |fn|
          value = if fn.kind_of?(Array) && value
            m = value["#{fn[0]}_attributes"]
            m[options[:_field_counter]] if m
          else
            value["#{fn}_attributes"] if value
          end
        end
        # TODO: find out field name here
        return value[options[:_model_field_name]] if value
      end
    else
      params[options[:_name]]
    end
  end

  def get_error_for_field_name(options)
    return unless options[:_model_field_name]
    names      = get_field_names_chain_from_nested_field_param_and_model(options)
    model_name = names.shift

    if !options[:model].blank? && model = get_target_model(names, options)
      model.errors[options[:_model_field_name]]
    end
  end

  def get_field_names_chain_from_nested_field_param_and_model(options)
    result = [options[:model].class.to_s.underscore]

    # associated model
    if assoc = options[:_nested_field]
      assoc.each do |a|
        if a[1] == :has_one
          result << a[0]
        else
          result << [a[0], a[2]]
        end
      end

    # serialized hash
    elsif options[:_name]
      result = []
      options[:_name].split("[").map { |n| n.chomp("]") }.each do |n|
        n = if n =~ /\A\d+\Z/
          n.to_i
        else
          n.to_sym
        end
        result << n
      end
    end
    result
  end

  def get_target_model(names, options)
    model = options[:model]
    return model unless options[:_nested_field]

    names.each_with_index do |fn,i|
      return nil if model.nil?
      model = if fn.kind_of?(Array)
        collection = model.send(fn[0])
        if fn[1]
          collection.select { |m| fn[1] == m.id }.first
        else
          collection[options[:_field_counter]]
        end
      else
        model.send(fn)
      end
    end
    model
  end

  private

    def get_new_or_deprecated_attr_and_warn(old_attr_name, new_attr_name, options, tag_name) # DEPRECATED v0.1.8
      if options[old_attr_name]
        Rails.logger.warn "DEPRECATION WARNING: #{tag_name}: use of `#{old_attr_name}` is deprecated, use `#{new_attr_name}`"
        return options[old_attr_name]
      else
        return options[new_attr_name]
      end
    end

    def derive_tag_and_css_class(tag, options)
      if tag && tag.to_s.include?(".")
        css_classes = tag.to_s.split(".")
        tag_name = css_classes.shift
        css_classes = css_classes.join(" ")
        tag_name = "div" if tag_name.empty?
        options[:class] = ((options[:class] || "").split(" ") + [css_classes]).join(" ")
        [tag_name, options]
      else
        [tag, options]
      end
    end

    def prepare_attr_map(map)
      return nil unless map
      map = map.split(/,\s*?/)
      map.map! do |a|
        if a.include?(":")
          a
        else
          "#{a.strip}:data-#{a.strip.gsub("_","-")}"
        end
      end
      map.join(",")
    end

    def read_attr_map(map)
      return {} unless map
      attr_map_hash = {}
      map.split(",").each do |attr|
        attr = attr.split(":")
        attr_map_hash[attr[0].to_sym] = attr[1]
      end
      attr_map_hash
    end

    # If options comes with :mapped_attrs hash, then
    # this method will create (or update) :attr_map with
    # correct attribute names and move all key/value pairs from :mapped_attrs
    # to the top level `options` Hash. This saves typing & code duplication
    # when you have a lot of attributes to list but you also have a lot values to specify
    # for them.
    #
    # In other words, this method allows you to write this:
    #
    #   component_block :my_component, "div", { mapped_attrs: { attr1: "value1", attr2: "value2" }
    #
    # instead of this:
    #
    #   component_block :my_component, "div", { attr_map: "attr1,attr2", attr1: "value1", attr2: "value2" }
    def create_attr_map(options)
      options[:attr_map] ||= ""
      options[:mapped_attrs].each do |k,v|
        options[k] = v
        options[:attr_map] += ",#{k}"
      end
      options.delete(:mapped_attrs)
      options[:attr_map].sub(/\A,/, "")
      options
    end

end
