class WebfaceForm

  FILTERED_OPTIONS = [:label, :suffix, :hint, :nested_field, :serialized_hash, :model_name, :model_field_name, :radio_options_id, :popup_hint, :has_blank]

  class NoSuchFormFieldType < Exception;end

  def initialize(model, view_context, no_labels: false)
    @model            = model
    @view_context     = view_context
    @tab_index        = 0
    @tab_indexes_used = []
    @field_counter    = {}
    @no_labels        = no_labels
  end

  def method_missing(m, *args)

    field_type = m
    options    = args.last.kind_of?(Hash) ? args.last : {}

    unless self.class.private_method_defined?("_#{field_type}")
      raise NoSuchFormFieldType, "Field type `#{field_type}` not supported"
    end

    full_field_name = if args.first.blank? || args.first.to_s =~ /!\Z/
      args.first.to_s.sub("!", "")
    else
      "#{@model.class.to_s.underscore}[#{args.first}]"
    end

    if options[:nested_field]
      full_field_name = modify_name_for_nested_field(full_field_name, options[:nested_field])
    elsif args.first && args.first.to_s.include?("[") && options[:nested_field].nil?
      full_field_name = modify_name_for_serialized_field(full_field_name)
    end

    if @field_counter[full_field_name]
      @field_counter[full_field_name] += 1
    else
      @field_counter[full_field_name] = 0
    end
    options[:_field_counter] = @field_counter[full_field_name]


    unless m.to_sym == :hidden
      options[:tabindex] ||= @tab_index += 1
      while @tab_indexes_used.include?(options[:tabindex])
        options[:tabindex] = @tab_index += 1
      end
      @tab_indexes_used << options[:tabindex]
    end

    if options[:name]
      options[:radio_options_id] = options[:name]
    end

    options.merge!({ name: full_field_name, _name: full_field_name, _model_field_name: args.first, model: @model, model_name: self.model_name})

    if (options[:label].nil? && !@no_labels) || options[:label] == true
      options[:label] = I18n.t("models.#{field_name_for_i18n(full_field_name)}")
    end

    self.send("_#{field_type}", filter_options(options))
  end

  def model_name
    if @model
      @model.class.to_s.underscore
    else
      options[:name].split("[").first
    end
  end

  private

    def _text(attrs)
      @view_context.component :text_form_field, attrs
    end

    def _text_with_validation(attrs)
      @view_context.component :text_form_field_with_validation, attrs
    end

    def _password(attrs)
      @view_context.component :password_form_field, attrs
    end

    def _hidden(attrs)
      @view_context.component :hidden_form_field, attrs
    end

    def _numeric(attrs)
      @view_context.component :numeric_form_field, attrs
    end

    def _textarea(attrs)
      @view_context.component :textarea_form_field, attrs
    end

    def _checkbox(attrs)
      @view_context.component :checkbox, attrs
    end

    def _select(attrs)
      @view_context.component :select, attrs
    end

    def _editable_select(attrs)
      @view_context.component :editable_select, attrs
    end

    def _radio(attrs)
      @view_context.component :radio, attrs
    end

    def _submit(attrs)
      @view_context.component :button, attrs.merge({ caption: attrs.delete(:_model_field_name), type: :submit, prevent_native_click_event: false })
    end

    # Every option from FILTERED_OPTIONS is converted to an option
    # with the leading _ in its name. For example :suffix becomes :_suffix.
    # This will allow #component_block to automatically filter out these
    # options and not include them into the list of html attributes added
    # to the component's tag.
    def filter_options(options)
      new_options = {}
      options.delete_if do |name, value|
        if FILTERED_OPTIONS.include?(name)
          new_options["_#{name}".to_sym] = value
        end
      end
      options.merge(new_options)
    end

    def modify_name_for_nested_field(name, association_name)
      if association_name.kind_of?(Array)
        association_name.each do |a|
          name = send("modify_name_for_nested_#{a[1]}", name, a[0])
        end
        return name
      else
        modify_name_for_nested_has_many(name, association_name)
      end
    end

    def modify_name_for_serialized_field(field_name)
      model,remainder = field_name.split("[",2)
      remainder.chomp!("]")
      field_name,remainder = remainder.split("[",2)
      "#{model}[#{field_name}][#{remainder}"
    end

    def modify_name_for_nested_has_one(name, nested_has_one_name)
      # name   == model[attribute]
      # result == model[association][attribute]
      model_name        = name.scan(/\A.*?\[/)[0].sub("[", "")
      field_names       = name.scan(/\[.*?\]/)
      actual_field_name = field_names.pop
      "#{model_name}#{field_names.join("")}[#{nested_has_one_name}_attributes]#{actual_field_name}"
    end

    def modify_name_for_nested_has_many(name, nested_has_many_name)
      # name   == model[attribute]
      # result == model[association][][attribute]
      model_name        = name.scan(/\A.*?\[/)[0].sub("[", "")
      field_names       = name.scan(/\[.*?\]/)
      actual_field_name = field_names.pop
      "#{model_name}#{field_names.join("")}[#{nested_has_many_name}_attributes][]#{actual_field_name}"
    end

    def field_name_for_i18n(fn)
      fn = fn.split("[").map { |n| n.sub("]", "") }.join(".")
    end

end
